package com.cybersoft.springbootdemo.config;

import com.cybersoft.springbootdemo.model.UserModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//Thông báo cho Spring boot biết khi chạy ở tầng config thì phải quét
// và cấu hình
@Configuration
public class CustomConfig {
    @Bean// Dinh nghia class sau do dua len IOC container de dung chung
    public UserModel userModel(){
        UserModel userModel = new UserModel();
        userModel.setUsername("Nguyen Van A");
        userModel.setPassword("123456");
        return userModel;
    }
}
