package com.cybersoft.springbootdemo.controller;

import com.cybersoft.springbootdemo.model.StudentModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    List<StudentModel> studentList = new ArrayList<>();;
    @PostMapping("/add")
    List<StudentModel> addStudentByRequestParam(@RequestParam String name, @RequestParam int age){
        studentList.add(new StudentModel(name,age));
        return studentList;
    }

    @PostMapping("/add/{name}/{age}")
    List<StudentModel> addStudentByPathVariable(@PathVariable String name, @PathVariable int age){
        studentList.add(new StudentModel(name,age));
        return studentList;
    }

    @PostMapping("/add/requestBody")
    List<StudentModel> addStudentByRequestBody(@RequestBody StudentModel student){
        studentList.add(student);
        return studentList;
    }
}
