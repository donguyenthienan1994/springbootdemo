package com.cybersoft.springbootdemo.controller;

import org.springframework.boot.autoconfigure.gson.GsonBuilderCustomizer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {
//    /calculator => GET
//    nhan 2 tham so soA va soB
//    xuat ra tong 2 tham so
    @GetMapping
    public int sum(@RequestParam("soA") int so1,@RequestParam("soB") int so2){

        return so1 + so2;
    }
}
