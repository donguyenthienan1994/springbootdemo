package com.cybersoft.springbootdemo.controller;

import com.cybersoft.springbootdemo.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/*
    Annotation : @
    @Controller : Thông báo cho Spring boot biết class dùng để định nghĩa đường dẫn có trả ra giao diện
    @Restbody : Chỉ trả ra text
    @RestController : Định nghĩa đường dẫn chỉ trả ra text
    @RequestMapping : Khai bao duong dan
*/
@RestController
@RequestMapping("/hello")//url cha
public class HelloController {

    @Autowired//Lay class dc luu tru tren IOC xuong de su dung
//    @Qualifier("ten_bean") : Giup lay class co ten bean duoc chi dinh tren IOC
    UserModel userModel;

    @GetMapping
    public ResponseEntity<?> hello(){
        List<UserModel> userModelList = new ArrayList<>();
        userModelList.add(userModel);
        userModel.setUsername("Tran Van B");
        userModelList.add(userModel);
        return new ResponseEntity<List<UserModel>>(userModelList, HttpStatus.OK);
    }

    @GetMapping("/loichao")//url con
    public String loichao(){

        return "Day la sub link cua /hello : " + userModel.getUsername();
    }
}
